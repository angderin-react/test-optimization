import React, { useCallback, useState } from "react";

import Button from "./components/UI/Button/Button.js";

import "./App.css";
import DemoOutput from "./components/UI/Button/demo/DemoOutput.js";

function App() {
  const [isShowParagraph, setIsShowParagraph] = useState(false);
  const [isAllowToggle, setAllowToggle] = useState(false);

  console.log("APP_RUNNING");

  const handlerToggleParagraph = useCallback(() => {
    if (isAllowToggle) {
      setIsShowParagraph((prevShowParagraph) => !prevShowParagraph);
    }
  }, [isAllowToggle]);

  const handlerAllowToggle = () => {
    setAllowToggle(true);
  };

  return (
    <div className="app">
      <h1>Hi there!</h1>
      <DemoOutput show={isShowParagraph} />
      <Button onClick={handlerAllowToggle}>Allow Toggling</Button>
      <Button onClick={handlerToggleParagraph}>Toggle Paragraph</Button>
    </div>
  );
}

export default App;
